package be.multimedi.restSamples.webshop.models;

import be.multimedi.restSamples.webshop.entities.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class UserList {
   List<User> users;

   public UserList(List<User> users) {
      this.users = users;
   }

   public UserList() {
   }

   public List<User> getUsers() {
      return users;
   }

   public void setUsers(List<User> users) {
      this.users = users;
   }
}
