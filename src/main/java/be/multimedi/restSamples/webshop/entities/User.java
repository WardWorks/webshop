package be.multimedi.restSamples.webshop.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Table(name= "WebshopUsers")
@JsonIgnoreProperties(value="hibernateLazyInitializer")
public class User {

   @Id
   private String email;

   @Column(nullable = false)
   private String password;

   @Column(nullable = false)
   @NotBlank
   private String firstname;
   @Column(nullable = false)
   @NotBlank
   private String lastname;

   //#Adres
   private String street;
   private String busNr;
   private String zipcode;
   private String city;
   private String country;

   @OneToMany(mappedBy = "user")
   private List<Order> orders;

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   //@JsonIgnore
   @JsonInclude(JsonInclude.Include.NON_NULL)
   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public String getFirstname() {
      return firstname;
   }

   public void setFirstname(String firstname) {
      this.firstname = firstname;
   }

   public String getLastname() {
      return lastname;
   }

   public void setLastname(String lastname) {
      this.lastname = lastname;
   }

   public String getStreet() {
      return street;
   }

   public void setStreet(String street) {
      this.street = street;
   }

   public String getBusNr() {
      return busNr;
   }

   public void setBusNr(String busNr) {
      this.busNr = busNr;
   }

   public String getZipcode() {
      return zipcode;
   }

   public void setZipcode(String zipcode) {
      this.zipcode = zipcode;
   }

   public String getCity() {
      return city;
   }

   public void setCity(String city) {
      this.city = city;
   }

   public String getCountry() {
      return country;
   }

   public void setCountry(String country) {
      this.country = country;
   }

   @JsonIgnore
   public List<Order> getOrders() {
      return orders;
   }

   public void setOrders(List<Order> orders) {
      this.orders = orders;
   }
}
