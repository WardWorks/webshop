package be.multimedi.restSamples.webshop.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "WebshopOrders")
@JsonIgnoreProperties(value="hibernateLazyInitializer")
public class Order {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "email")
   private User user;

   @OneToMany(mappedBy = "order", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
   private List<OrderItem> items;

   @Column(nullable = false)
   private LocalDateTime date;

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public User getUser() {
      return user;
   }

   public void setUser(User user) {
      this.user = user;
   }

   public List<OrderItem> getItems() {
      return items;
   }

   public void setItems(List<OrderItem> items) {
      this.items = items;
   }

   public LocalDateTime getDate() {
      return date;
   }

   public void setDate(LocalDateTime date) {
      this.date = date;
   }
}
