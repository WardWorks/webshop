package be.multimedi.restSamples.webshop.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Table(name="WebshopOrderItems")
public class OrderItem {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name="productId", nullable = false)
   private Product product;

   @JsonIgnore
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "orderId", nullable = false)
   private Order order;

   @Column(nullable = false)
   @Min(0)
   private int amount;

   public OrderItem(Product product, Order order, @Min(0) int amount) {
      this.product = product;
      this.order = order;
      this.amount = amount;
   }

   public OrderItem() {
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public Product getProduct() {
      return product;
   }

   public void setProduct(Product product) {
      this.product = product;
   }

   public Order getOrder() {
      return order;
   }

   public void setOrder(Order order) {
      this.order = order;
   }

   public int getAmount() {
      return amount;
   }

   public void setAmount(int amount) {
      this.amount = amount;
   }
}
