package be.multimedi.restSamples.webshop.controllers;

import be.multimedi.restSamples.webshop.entities.User;
import be.multimedi.restSamples.webshop.models.UserList;
import be.multimedi.restSamples.webshop.repositories.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

   @Autowired
   UserDAO dao;

   @GetMapping( produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
   public ResponseEntity<UserList> getAllHandler(){
      List<User> users=  dao.findAll();
      users.forEach((u)->u.setPassword(null));
      return ResponseEntity.ok(new UserList(users));
   }

   //#todo: email:regex
   @GetMapping( path = "/{email}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
   public ResponseEntity<User> getByIdHandler(@Email @PathVariable("email") String email){
      User user = dao.getUserByEmail(email);
      user.setPassword(null);
      return user != null ? ResponseEntity.ok(user) : ResponseEntity.badRequest().build();
   }

   //#todo: email:regex
   @DeleteMapping( path = "/{email}")
   public ResponseEntity<?> deleteHandler( @PathVariable("email") String email){
      dao.deleteById(email);
      return ResponseEntity.ok().build();
   }

//   @PostMapping( consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE },
//                  produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE } )
//   public ResponseEntity<User> postHandler( @RequestBody User user){
////      if(user != null && !user.getEmail().isBlank()){
//         dao.save(user);
//         user.setPassword(null);
//         return ResponseEntity.ok(user);
////      }
////      return ResponseEntity.badRequest().build();
//   }

   //#todo: email:regex
   @PutMapping( path = "/{email}", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE },
           produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE } )
   public ResponseEntity<User> putHandler(@PathVariable("email") String email, @RequestBody User user) {
      if (user != null && user.getEmail().equals(email)) {
         dao.save(user);
         user.setPassword(null);
         return ResponseEntity.ok(user);
      }
      return ResponseEntity.badRequest().build();
   }
}

