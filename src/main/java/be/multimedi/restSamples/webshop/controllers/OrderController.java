package be.multimedi.restSamples.webshop.controllers;

import be.multimedi.restSamples.webshop.entities.Order;
import be.multimedi.restSamples.webshop.entities.OrderItem;
import be.multimedi.restSamples.webshop.models.OrderItemList;
import be.multimedi.restSamples.webshop.models.OrderList;
import be.multimedi.restSamples.webshop.repositories.OrderDAO;
import be.multimedi.restSamples.webshop.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.CollectionTable;
import java.util.List;

@RestController
//@Controller
//@ResponseBody
@RequestMapping("/orders")
public class OrderController {

   @Autowired
   OrderDAO dao;

   @Autowired
   OrderService orderService;

   @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<OrderList> getAllHandler() {
      List<Order> orders = dao.findAll();
      return ResponseEntity.ok(new OrderList(orders));
   }

   @GetMapping(path = "/{id:^\\d+$}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<Order> getByIdHandler(@PathVariable("id") int id) {
      Order o = dao.getOrderById(id);
      return o == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(o);
   }

   //#todo: test Post
   @PostMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
           consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<Order> postHandler(@RequestBody Order order) {
      if (order != null && order.getId() <= 0) {
         //dao.save(order);
         order = orderService.placeOrder(order);
         return order != null ? ResponseEntity.ok(order) : ResponseEntity.badRequest().build();
      }
      return ResponseEntity.badRequest().build();
   }

   @PostMapping(path = "/orderOne", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
           consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<Order> postOrderParameterHandler(@RequestParam String email, @RequestParam int productId, @RequestParam int amount) {
      System.out.println(email);
      System.out.println(productId);
      System.out.println(amount);
      int orderId = orderService.orderProduct(email, productId, amount);
      if (orderId == 0) {
         return ResponseEntity.badRequest().build();
      }
      return ResponseEntity.ok(dao.getOrderById(orderId));
   }

   //#todo: test Post
   @PostMapping(path="/orderMany", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
           consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<Order> postOrderParameterHandler(@RequestParam String email, @RequestBody OrderItemList items) {
      int[][] products = new int[items.getItems().size()][2];
      int index = 0;
      for (OrderItem item : items.getItems()) {
         products[index][0] = item.getProduct().getId();
         products[index][1] = item.getAmount();
         index++;
      }
      int orderId = orderService.orderProducts(email, products);
      return orderId == 0 ? ResponseEntity.badRequest().build() : ResponseEntity.ok(dao.getOrderById(orderId));
   }

}
