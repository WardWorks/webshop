package be.multimedi.restSamples.webshop.controllers;

import be.multimedi.restSamples.webshop.entities.Brand;
import be.multimedi.restSamples.webshop.models.BrandList;
import be.multimedi.restSamples.webshop.repositories.BrandDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/brands")
//@Transactional
public class BrandController {
   @Autowired
   BrandDAO dao;

   @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<BrandList> getAllHandler(){
      return ResponseEntity.ok(new BrandList(dao.findAll()));
   }

   @GetMapping(path = "/{id:^\\d+$}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
   public ResponseEntity<Brand> getByIdHandler( @PathVariable("id") int id){
      Brand brand = dao.getBrandById(id);
      if(brand != null){
         return ResponseEntity.ok(brand);
      }
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
   }

   @DeleteMapping(path = "/{id:^\\d+$}")
   public ResponseEntity<?> deleteHandler(@PathVariable int id){
      dao.deleteById(id);
      return ResponseEntity.ok().build();
   }

   @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
                  produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
   public ResponseEntity<Brand> postHandler(@RequestBody Brand brand){
      if( brand.getId() <= 0){
         dao.save(brand);
         return ResponseEntity.ok(brand);
      }
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
   }
}
