package be.multimedi.restSamples.webshop.repositories;

import be.multimedi.restSamples.webshop.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDAO extends JpaRepository<Product, Integer> {
   default Product getProductById(Integer id){
      return findById(id).orElse(null);
   }
}
