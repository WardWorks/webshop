package be.multimedi.restSamples.webshop.repositories;

import be.multimedi.restSamples.webshop.entities.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductCategoryDAO extends JpaRepository<ProductCategory, Integer> {
   default ProductCategory getProductCategoryById(Integer id){
      return findById(id).orElse(null);
   }
}
