package be.multimedi.restSamples.webshop.repositories;


import be.multimedi.restSamples.webshop.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDAO extends JpaRepository<Order, Integer> {
   default Order getOrderById(Integer id){
      return findById(id).orElse(null);
   }
}
