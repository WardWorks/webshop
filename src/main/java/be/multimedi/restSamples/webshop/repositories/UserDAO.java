package be.multimedi.restSamples.webshop.repositories;

import be.multimedi.restSamples.webshop.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO extends JpaRepository<User, String> {
   default User getUserByEmail(String email){
      return findById(email).orElse(null);
   }
}
