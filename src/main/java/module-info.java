open module be.multimedi.restSamples.webshop{
   //#Spring core
   requires spring.core;
   requires spring.context;
   requires spring.beans;

   //#Spring boot
   requires spring.boot;
   requires spring.boot.autoconfigure;
   requires spring.boot.starter.web;
   requires spring.boot.starter.jdbc;
   requires spring.boot.starter.data.jpa;
   requires spring.boot.starter.aop;

   //#Hibernate
   requires spring.data.commons;
   requires spring.data.jpa;
   requires spring.jdbc;
   requires spring.tx;
   requires java.sql;
   requires java.persistence;
   requires org.aspectj.weaver;
   requires com.zaxxer.hikari;

   //#Rest
   requires spring.web;
   requires java.annotation;
   requires java.xml.bind;
   requires com.sun.xml.bind;
   requires net.bytebuddy;
   requires tomcat.embed.core;
   requires java.validation;
   requires jackson.annotations;
   requires com.fasterxml.jackson.dataformat.xml;
   requires spring.data.rest.webmvc;
   requires spring.webmvc;
   requires spring.data.rest.core;
   //#todo: afwerken

   //#Spring security
//   requires spring.boot.starter.security
//   requires spring.security.core;
//   requires spring.security.web;
//   requires spring.security.config;
}
