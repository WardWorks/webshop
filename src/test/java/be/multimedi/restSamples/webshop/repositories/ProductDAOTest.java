package be.multimedi.restSamples.webshop.repositories;

import be.multimedi.restSamples.webshop.entities.Product;
import be.multimedi.restSamples.webshop.entities.ProductCategory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class ProductDAOTest {
   @Autowired
   ProductDAO dao;

   @Test
   public void testGetAll(){
      List<Product> products = dao.findAll();

      assertNotNull(products);
      assertEquals(3, products.size());
   }

   @Test
   public void testGetById(){
      Product product = dao.getProductById(1);

      assertNotNull(product);
      assertEquals("XS MAX", product.getModel());
      assertEquals(1400, product.getPrice());
      assertEquals(20, product.getStock());
      assertEquals("Apple", product.getBrand().getName());
      assertEquals("smartphones", product.getCategory().getName());
   }

   //@DirtiesContext
//   @Test
//   @Rollback
//   public void testDeleteById(){
//      dao.deleteById(1);
//      List<Product> products = dao.findAll();
//
//      assertNotNull(products);
//      assertEquals(2, products.size());
//   }
}
