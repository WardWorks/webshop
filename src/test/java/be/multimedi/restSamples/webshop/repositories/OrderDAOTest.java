package be.multimedi.restSamples.webshop.repositories;

import be.multimedi.restSamples.webshop.entities.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class OrderDAOTest {
   @Autowired
   OrderDAO dao;

   @Test
   public void testGetAll() {
      List<Order> orders = dao.findAll();

      assertNotNull(orders);
      assertEquals(3, orders.size());
   }

   @Test
   public void testGetById() {
      Order order = dao.getOrderById(1);

      assertNotNull(order);
      assertEquals(LocalDateTime.of(2010, 12, 31, 12, 30, 0), order.getDate());
      assertEquals("ward@multimedi.be", order.getUser().getEmail());
      assertEquals(1, order.getItems().size());
   }

   //@DirtiesContext
   @Test
   @Rollback
   public void testDeleteById(){
      dao.deleteById(1);
      List<Order> orders = dao.findAll();

      assertNotNull(orders);
      assertEquals(2, orders.size());
   }
}
