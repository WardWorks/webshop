package be.multimedi.restSamples.webshop.repositories;

import be.multimedi.restSamples.webshop.entities.Brand;
import be.multimedi.restSamples.webshop.entities.ProductCategory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class ProductCategoryDAOTest {
   @Autowired
   ProductCategoryDAO dao;

   @Test
   public void testGetAll(){
      List<ProductCategory> categories = dao.findAll();

      assertNotNull(categories);
      assertEquals(3, categories.size());
   }

   @Test
   public void testGetById() {
      ProductCategory category = dao.getProductCategoryById(1);
      assertNotNull(category);

      assertEquals("laptops", category.getName());
      assertEquals(null, category.getDescription());
   }

   //@DirtiesContext
   @Test
   @Rollback
   public void testSave(){
      ProductCategory c = new ProductCategory();
      c.setName("TestBrand");
      c.setDescription("A brand to test with");

      dao.save(c);
      List<ProductCategory> categories = dao.findAll();

      assertNotNull(categories);
      assertEquals(4, categories.size());
   }

   //@DirtiesContext
//   @Test
//   @Rollback
//   public void testDeleteById(){
//      dao.deleteById(1);
//      List<ProductCategory> categories = dao.findAll();
//
//      assertNotNull(categories);
//      assertEquals(2, categories.size());
//   }
}
