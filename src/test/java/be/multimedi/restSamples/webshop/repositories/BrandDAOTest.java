package be.multimedi.restSamples.webshop.repositories;

import be.multimedi.restSamples.webshop.entities.Brand;
import be.multimedi.restSamples.webshop.entities.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class BrandDAOTest {
   @Autowired
   BrandDAO dao;

   @Test
   public void testGetAll(){
      List<Brand> brands = dao.findAll();

      assertNotNull(brands);
      assertEquals(3, brands.size());
   }

   @Test
   public void testGetById(){
      Brand brand = dao.getBrandById(1);

      assertNotNull(brand);
      assertEquals("Apple", brand.getName());
      assertEquals("www.apple.com", brand.getWebsite());
   }

   //@DirtiesContext
   @Test
   @Rollback
   public void testSave(){
      Brand b = new Brand();
      b.setName("TestBrand");
      b.setWebsite("www.nowhere.com");

      dao.save(b);
      List<Brand> brands = dao.findAll();

      assertNotNull(brands);
      assertEquals(4, brands.size());
   }

   //@DirtiesContext
//   @Test
//   @Rollback
//   public void testDeleteById(){
//      dao.deleteById(1);
//      List<Brand> brands = dao.findAll();
//
//      assertNotNull(brands);
//      assertEquals(2, brands.size());
//   }
}
